/*
 * GStreamer
 *
 * Copyright (C) 2012 Texas Instruments
 * Copyright (C) 2012 Collabora Ltd
 *
 * Authors:
 *  Alessandro Decina <alessandro.decina@collabora.co.uk>
 *  Rob Clark <rob.clark@linaro.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdint.h>
#include <gst/gst.h>

#include <omap_drm.h>
#include <omap_drmif.h>
#include <xf86drmMode.h>

#include "gstducatibufferpriv.h"

#define GST_DUCATI_BUFFER_PRIV_QUARK gst_ducati_buffer_priv_quark_get_type()
static GST_BOILERPLATE_QUARK (GstDucatiBufferPriv, gst_ducati_buffer_priv_quark);

#define DUCATI_BUFFER_PRIV_QUARK ducati_buffer_priv_quark_get_type()
static GST_BOILERPLATE_QUARK (DucatiBufferPriv, ducati_buffer_priv_quark);

void
gst_ducati_buffer_priv_set (GstBuffer * buf, GstDucatiBufferPriv * priv)
{
  gst_buffer_set_qdata (buf, GST_DUCATI_BUFFER_PRIV_QUARK,
      gst_structure_id_new (GST_DUCATI_BUFFER_PRIV_QUARK,
          DUCATI_BUFFER_PRIV_QUARK, GST_TYPE_DUCATI_BUFFER_PRIV, priv, NULL));
}

GstDucatiBufferPriv *
gst_ducati_buffer_priv_get (GstBuffer * buf)
{
  const GstStructure *s;
  const GValue *val;

  s = gst_buffer_get_qdata (buf, GST_DUCATI_BUFFER_PRIV_QUARK);
  if (s == NULL)
    return NULL;

  val = gst_structure_id_get_value (s, DUCATI_BUFFER_PRIV_QUARK);
  if (val == NULL)
    return NULL;

  return GST_DUCATI_BUFFER_PRIV (gst_value_get_mini_object (val));
}

GstDucatiBufferPriv *
gst_ducati_buffer_priv_new (void)
{
  return (GstDucatiBufferPriv *) gst_mini_object_new (GST_TYPE_DUCATI_BUFFER_PRIV);
}

static void
gst_ducati_buffer_priv_finalize (GstMiniObject * mini_obj)
{
  GstDucatiBufferPriv *priv = (GstDucatiBufferPriv *) mini_obj;

  omap_bo_del (priv->bo);

  /* not chaining up to GstMiniObject's finalize for now, we know it's empty */
}

GST_BOILERPLATE_MINI_OBJECT(GstDucatiBufferPriv, gst_ducati_buffer_priv);

static void
gst_ducati_buffer_priv_class_init (GstDucatiBufferPrivClass * klass)
{
  GST_MINI_OBJECT_CLASS (klass)->finalize = gst_ducati_buffer_priv_finalize;
}
