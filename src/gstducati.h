/*
 * GStreamer
 * Copyright (c) 2010, Texas Instruments Incorporated
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef __GST_DUCATI_H__
#define __GST_DUCATI_H__

#include <stdint.h>
#include <string.h>

#include <dce.h>
#include <xdc/std.h>
#include <ti/sdo/ce/Engine.h>
#include <ti/sdo/ce/video3/viddec3.h>

#include <gst/gst.h>

G_BEGIN_DECLS

GST_DEBUG_CATEGORY_EXTERN (gst_ducati_debug);
#define GST_CAT_DEFAULT gst_ducati_debug

/* align x to next highest multiple of 2^n */
#define ALIGN2(x,n)   (((x) + ((1 << (n)) - 1)) & ~((1 << (n)) - 1))

G_END_DECLS

#ifndef GST_DISABLE_GST_DEBUG
extern void gst_ducati_set_generic_error_strings (const char * strings[]);
extern void gst_ducati_log_extended_error_info (uint32_t error, const char *strings[]);
#else
#define gst_ducati_log_extended_error_info(f) ((void)0)
#endif

#endif /* __GST_DUCATI_H__ */
